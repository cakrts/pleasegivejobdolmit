package ck.message.service;

import ck.message.model.entity.Message;
import ck.message.model.MessageAndCount;
import ck.message.model.MessageDTO;
import ck.message.repositories.MessageRepository;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

  @Autowired private MessageRepository messageRepository;

  @Autowired private ModelMapper modelMapper;

  @Override
  @Transactional
  public void saveMessage(String message) {
    MessageDTO messageToSave = new MessageDTO(message);
    messageRepository.save(convertToEntity(messageToSave));
  }

  @Override
  @Transactional
  public MessageAndCount getLastMessage() {
    return new MessageAndCount(
        messageRepository.count(), convertToDto(messageRepository.findTopByOrderByIdDesc()));
  }

  @Override
  @Transactional
  public List<MessageDTO> getAllMessages() {
    return messageRepository
        .findAll()
        .stream()
        .map(this::convertToDto)
        .collect(Collectors.toList());
  }

  private MessageDTO convertToDto(Message message) {
    return modelMapper.map(message, MessageDTO.class);
  }

  private Message convertToEntity(MessageDTO messageDTO) {
    return modelMapper.map(messageDTO, Message.class);
  }
}
