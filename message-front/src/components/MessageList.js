import React, { Component } from 'react';
import './App.css';
import { Container, Table } from 'reactstrap';
import AppNavbar from './AppNavBar';
import MessageService from '../service/MessageService';

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.state = { messages: [], isLoading: true };
        this.fetchMessages = this.fetchMessages.bind(this)
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        this.fetchMessages();
    }

    fetchMessages() {
        MessageService.getAllMessages()
            .then(response => {
                this.setState({ messages: response.data, isLoading: false })
            }).catch(error => console.log(error));
    }

    render() {
        const { messages, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const messageList = messages.map(message => <tr key={message.id}><td>{message.message}</td></tr>);

        return (
            <div>
                <AppNavbar />
                <Container fluid>
                    <h3>All messages are displayed here</h3>
                    <Table className="mt-4">
                        <tbody>
                            {messageList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default MessageList;