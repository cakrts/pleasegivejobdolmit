package ck.message.model;

public class MessageAndCount {

  private long count;
  private MessageDTO message;

  MessageAndCount() {}

  public MessageAndCount(long count, MessageDTO message) {
    this.count = count;
    this.message = message;
  }

  public long getCount() {
    return count;
  }

  public MessageDTO getMessage() {
    return message;
  }
}
