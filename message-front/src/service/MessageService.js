import axios from 'axios'

const ALLMESSAGES = '/messages'
const SAVE = '/save'
const LASTMESSAGE = '/lastmessage'

class MessageService {

    getAllMessages() {
        return axios.get(ALLMESSAGES);
    }

    getLastMessage() {
        return axios.get(LASTMESSAGE);
    }

    postNewMessage(input) {
        return axios.post(SAVE, {input});
    }

}
export default new MessageService()