package ck.message.service;

import ck.message.model.MessageAndCount;

import ck.message.model.MessageDTO;
import java.util.List;

public interface MessageService {

  void saveMessage(String message);

  MessageAndCount getLastMessage();

  List<MessageDTO> getAllMessages();
}
