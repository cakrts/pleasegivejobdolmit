import React, { Component } from "react";
import './App.css';
import MessageService from "../service/MessageService";
import { Button, Form } from 'reactstrap';


class Input extends Component {
    constructor(props) {
        super(props);
        this.state = { input: '' };
    }

    handleChange = event => {
        this.setState({ input: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();
        MessageService.postNewMessage(this.state.input).then(res => { console.log(res.data); }).catch(error => console.log(error));
    }

    refreshPage() {
        window.location.reload();
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <textarea type="text" name="input" onChange={this.handleChange} />
                    <div>
                        <Button type="submit" onClick={this.refreshPage}>Save</Button>
                    </div>
                </Form>
            </div>
        )
    }
}

export default Input;