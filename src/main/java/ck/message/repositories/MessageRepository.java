package ck.message.repositories;

import ck.message.model.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
  @Override
  long count();

  Message findTopByOrderByIdDesc();
}
