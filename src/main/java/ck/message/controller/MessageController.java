package ck.message.controller;

import ck.message.model.Input;
import ck.message.model.MessageAndCount;
import ck.message.model.MessageDTO;
import ck.message.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {

  private final Logger log = LoggerFactory.getLogger(MessageController.class);

  @Autowired private MessageService messageService;

  @RequestMapping(value = "", method = RequestMethod.GET)
  public String index() {
    return "This is a MessageApplication startup response";
  }

  @RequestMapping(
      value = "/save",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public String saveMessage(@RequestBody Input input) {
    log.info("Received a message from POST request");
    messageService.saveMessage(input.getInput());
    return "";
  }

  @RequestMapping(value = "/messages", method = RequestMethod.GET)
  public List<MessageDTO> getAllMessages() {
    log.info("Returning all messages for GET request");
    return messageService.getAllMessages();
  }

  @RequestMapping(value = "/lastmessage", method = RequestMethod.GET)
  public MessageAndCount getLastMessage() {
    log.info("Returning the last message in database and total count");
    return messageService.getLastMessage();
  }
}
