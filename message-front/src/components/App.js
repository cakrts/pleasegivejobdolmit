import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MessageList from './MessageList';
import Message from './Message';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={Home} />
          <Route path='/messagelist' exact={true} component={MessageList} />
          <Route path='/messaging' exact={true} component={Message} />
        </Switch>
      </Router>
    )
  }
}

export default App;
