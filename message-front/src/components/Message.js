import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavBar';
import { Container, Table } from 'reactstrap';
import MessageService from '../service/MessageService';
import Input from './Input';

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = { lastMessage: '', isLoading: true };
        this.getLastMessage = this.getLastMessage.bind(this)
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        this.getLastMessage();
    }

    getLastMessage() {
        MessageService.getLastMessage().then(
            response => { this.setState({ lastMessage: response.data, isLoading: false }); }
        ).catch(error => console.log(error));
    }

    render() {
        const { lastMessage, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const messageFromDb = <tr><td>{lastMessage.message.message}</td><td>{lastMessage.message.created}</td></tr>;
        return (
            <div>
                <AppNavbar />
                <Container fluid>
                    <h3>Last message and message entry</h3>
                    <Table>
                        <thead>
                            <tr>
                                <th width="30%">Last Message</th>
                                <th>Total number of messages: {lastMessage.count}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {messageFromDb}
                        </tbody>
                    </Table>
                    <Input />
                </Container>
            </div>
        );
    }
}

export default Message;