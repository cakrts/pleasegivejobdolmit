package ck.message.model;

import java.time.LocalDateTime;

public class MessageDTO {

  private Long id;
  private String message;
  private LocalDateTime created;

  public MessageDTO() {}

  public MessageDTO(String message) {
    this.message = message;
  }

  public MessageDTO(Long id, String message, LocalDateTime created) {
    this.id = id;
    this.message = message;
    this.created = created;
  }

  public Long getId() {
    return id;
  }

  public String getMessage() {
    return message;
  }

  public LocalDateTime getCreated() {
    return created;
  }
}
