package ck.message.model.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "message")
public class Message {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "message")
  private String message;

  @Column(name = "created")
  private LocalDateTime created;

  public Message() {}

  public Message(Long id, String message, LocalDateTime created) {
    this.id = id;
    this.message = message;
    this.created = created;
  }

  public Long getId() {
    return id;
  }

  public String getMessage() {
    return message;
  }

  public LocalDateTime getCreated() {
    return created;
  }
}
