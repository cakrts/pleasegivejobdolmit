package ck.message;

import static org.junit.Assert.assertEquals;

import ck.message.model.MessageDTO;
import ck.message.model.entity.Message;
import java.time.LocalDateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration.AccessLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ModelTest {

  @TestConfiguration
  static class ServiceTestContextConfiguration {
    @Bean
    public ModelMapper modelMapper() {
      ModelMapper modelMapper = new ModelMapper();
      modelMapper
          .getConfiguration()
          .setFieldAccessLevel(AccessLevel.PRIVATE)
          .setFieldMatchingEnabled(true);
      return modelMapper;
    }
  }

  @Autowired private ModelMapper modelMapper;

  @Test
  public void entityToDto() {
    Message message = new Message(4L, "TESTING", LocalDateTime.now());
    MessageDTO messageDTO = modelMapper.map(message, MessageDTO.class);
    assertEquals("TESTING", messageDTO.getMessage());
    assertEquals(message.getCreated(), messageDTO.getCreated());
  }

  @Test
  public void dtoToEntity() {
    MessageDTO messageDTO = new MessageDTO(4L, "TESTING", LocalDateTime.now());
    Message message = modelMapper.map(messageDTO, Message.class);
    assertEquals("TESTING", message.getMessage());
    assertEquals(messageDTO.getCreated(), message.getCreated());
  }
}
